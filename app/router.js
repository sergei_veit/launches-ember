import EmberRouter from '@ember/routing/router';
import config from './config/environment';

const Router = EmberRouter.extend({
  location: config.locationType,
  rootURL: config.rootURL
});

Router.map(function () {
  this.route('latest');
  this.route('upcoming');
  this.route('all');
  this.route('view', { path: '/view/:id' });
  this.route('loading');
  this.route('error', { path: '/*path'});
});

export default Router;
