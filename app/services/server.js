import Service from '@ember/service';
import $ from 'jquery';
import Const from '../consts';

export default Service.extend({
  getQuery(type, params) {
    return {
      type,
      params,
      do() {
        return $.ajax({
          url: Const.get('server.host') + Const.get('server.paths.type'),
          data: params,
          method: 'get'
        }).done((json) => {
          return (
            typeof(json) === 'string'
              ? JSON.parse(json)
              : json
          );
        });
      }
    }
  },

  getPast(params) {
    return this.getQuery('past', params);
  },

  getUpcoming(params) {
    return this.getQuery('upcoming', params);
  },

  getAll(params) {
    return this.getQuery('all', params);
  }
});
