import EmberObject from '@ember/object';

const Options = {
  server: {
    host: 'https://api.spacexdata.com/v2/',
    paths: {
      past: 'launches',
      upcoming: 'launches/upcoming',
      all: 'launches/all'
    }
  }
};

export default EmberObject.extend(Options);
