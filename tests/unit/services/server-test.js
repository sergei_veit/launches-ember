import { module, test } from 'qunit';
import { setupTest } from 'ember-qunit';

module('Unit | Service | server', function (hooks) {
  setupTest(hooks);

  test('it exists', function (assert) {
    let service = this.owner.lookup('service:server');
    assert.ok(service);
  });

  test('getAll without params', function (assert) {
    let service = this.owner.lookup('service:server');
    const result = service.getAll();
    assert.equal(Object.keys(result).length, 3, "result has 3 items");
    assert.equal(typeof(result.do), "function", "result has 'do' function");
    assert.equal(result.params, undefined, "object don't have params");
    assert.equal(result.type, "all", "object have 'all' type");
  });

  test('getAll with params', function (assert) {
    let service = this.owner.lookup('service:server');
    const params = { a: 1 };
    const result = service.getAll(params);
    assert.equal(Object.keys(result).length, 3, "result has 3 items");
    assert.equal(typeof(result.do), "function", "result has 'do' function");
    assert.equal(result.params, params, "object have params");
    assert.equal(result.type, "all", "object have 'all' type");
  });

  test('getUpcoming without params', function (assert) {
    let service = this.owner.lookup('service:server');
    const result = service.getUpcoming();
    assert.equal(Object.keys(result).length, 3, "result has 3 items");
    assert.equal(typeof(result.do), "function", "result has 'do' function");
    assert.equal(result.params, undefined, "object don't have params");
    assert.equal(result.type, "upcoming", "object have 'upcoming' type");
  });

  test('getUpcoming with params', function (assert) {
    let service = this.owner.lookup('service:server');
    const params = { a: 1 };
    const result = service.getUpcoming(params);
    assert.equal(Object.keys(result).length, 3, "result has 3 items");
    assert.equal(typeof(result.do), "function", "result has 'do' function");
    assert.equal(result.params, params, "object have params");
    assert.equal(result.type, "upcoming", "object have 'upcoming' type");
  });

  test('getPast without params', function (assert) {
    let service = this.owner.lookup('service:server');
    const result = service.getPast();
    assert.equal(Object.keys(result).length, 3, "result has 3 items");
    assert.equal(typeof(result.do), "function", "result has 'do' function");
    assert.equal(result.params, undefined, "object don't have params");
    assert.equal(result.type, "past", "object have 'past' type");
  });

  test('getPast with params', function (assert) {
    let service = this.owner.lookup('service:server');
    const params = { a: 1 };
    const result = service.getPast(params);
    assert.equal(Object.keys(result).length, 3, "result has 3 items");
    assert.equal(typeof(result.do), "function", "result has 'do' function");
    assert.equal(result.params, params, "object have params");
    assert.equal(result.type, "past", "object have 'past' type");
  });
});
